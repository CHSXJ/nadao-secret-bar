// !!!!!!!!!!!!!!!!!!!!!! Please fix input (data:) part  accordingly krub :)
function submitAnswer() {
    $.ajax({
        url: '/api/submitAnswers',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            user_id: 3,
            answers: [
                { question_number: 1, answer: 'aaa' },
                { question_number: 2, answer: 'bbb' },
                { question_number: 3, answer: 'bbb' }
            ],
        }),
        dataType: 'json'
    }).done(function (data) {
        console.log("done");
        console.log(data);
        // window.location = 'index.php';
    }).fail(function (xhr, ajaxOptions, thrownError) {
        //xhr.responseText
        var responseMessage = xhr.responseText;
        if (xhr.status == 401) {
            //User not found
            var tmpObj = jQuery.parseJSON(xhr.responseText);
            responseMessage = tmpObj.error.message;
        }
        console.log("login fail: " + responseMessage);

    });
}

function getAnswers() {
    $.ajax({
        url: '/api/getAnswers',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            user_id: 1,
        }),
        dataType: 'json'
    }).done(function (data) {
        console.log("done");
        console.log(data);
        // window.location = 'index.php';
    }).fail(function (xhr, ajaxOptions, thrownError) {
        //xhr.responseText
        var responseMessage = xhr.responseText;
        if (xhr.status == 401) {
            //User not found
            var tmpObj = jQuery.parseJSON(xhr.responseText);
            responseMessage = tmpObj.error.message;
        }
        console.log("login fail: " + responseMessage);

    });
}

function getCorrectAnswers() {
    $.ajax({
        url: '/api/getCorrectAnswers',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            user_id: 1,
        }),
        dataType: 'json'
    }).done(function (data) {
        console.log("done");
        console.log(data);
        // window.location = 'index.php';
    }).fail(function (xhr, ajaxOptions, thrownError) {
        //xhr.responseText
        var responseMessage = xhr.responseText;
        if (xhr.status == 401) {
            //User not found
            var tmpObj = jQuery.parseJSON(xhr.responseText);
            responseMessage = tmpObj.error.message;
        }
        console.log("login fail: " + responseMessage);

    });
}

function getUserCorrectExceed() {
    $.ajax({
        url: '/api/getUserCorrectExceed',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            number_correct: 3,
        }),
        dataType: 'json'
    }).done(function (data) {
        console.log("done");
        console.log(data);
        // window.location = 'index.php';
    }).fail(function (xhr, ajaxOptions, thrownError) {
        //xhr.responseText
        var responseMessage = xhr.responseText;
        if (xhr.status == 401) {
            //User not found
            var tmpObj = jQuery.parseJSON(xhr.responseText);
            responseMessage = tmpObj.error.message;
        }
        console.log("login fail: " + responseMessage);

    });
}

function getImgGallery() {
    $.ajax({
        url: '/api/getImgGallery',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
        }),
        dataType: 'json'
    }).done(function (data) {
        console.log("done");
        console.log(data);
        // window.location = 'index.php';
    }).fail(function (xhr, ajaxOptions, thrownError) {
        //xhr.responseText
        var responseMessage = xhr.responseText;
        if (xhr.status == 401) {
            //User not found
            var tmpObj = jQuery.parseJSON(xhr.responseText);
            responseMessage = tmpObj.error.message;
        }
        console.log("login fail: " + responseMessage);

    });
}

function getTrailer() {
    $.ajax({
        url: '/api/getTrailerURL',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
        }),
        dataType: 'json'
    }).done(function (data) {
        console.log("done");
        console.log(data);
        // window.location = 'index.php';
    }).fail(function (xhr, ajaxOptions, thrownError) {
        //xhr.responseText
        var responseMessage = xhr.responseText;
        if (xhr.status == 401) {
            //User not found
            var tmpObj = jQuery.parseJSON(xhr.responseText);
            responseMessage = tmpObj.error.message;
        }
        console.log("login fail: " + responseMessage);

    });
}

function getTop100() {
    $.ajax({
        url: '/api/getTop100',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
        }),
        dataType: 'json'
    }).done(function (data) {
        console.log("done");
        console.log(data);
        // window.location = 'index.php';
    }).fail(function (xhr, ajaxOptions, thrownError) {
        //xhr.responseText
        var responseMessage = xhr.responseText;
        if (xhr.status == 401) {
            //User not found
            var tmpObj = jQuery.parseJSON(xhr.responseText);
            responseMessage = tmpObj.error.message;
        }
        console.log("login fail: " + responseMessage);

    });
}

function register() {
    $.ajax({
        url: '/api/user/register',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            user: 'Pkanjan2',
            pass: '12345678',
            gender: 0,
            firstname: 'pppp',
            lastname: 'kkkkk',
            province: 99,
            age: 27
        }),
        dataType: 'json'
    }).done(function (data) {
        console.log("done");
        window.location = window.location.href;
    }).fail(function (xhr, ajaxOptions, thrownError) {
        //xhr.responseText
        var responseMessage = xhr.responseText;
        if (xhr.status == 401) {
            //User not found
            var tmpObj = jQuery.parseJSON(xhr.responseText);
            responseMessage = tmpObj.error.message;
        }
        console.log("login fail: " + responseMessage);

    });
}

function login() {
    $.ajax({
        url: '/api/user/validateCredential',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            user: 'Pkanjan',
            pass: '12345678',
        }),
        dataType: 'json'
    }).done(function (data) {
        console.log("done");
        console.log(data);
        window.location = 'index.php';
    }).fail(function (xhr, ajaxOptions, thrownError) {
        //xhr.responseText
        var responseMessage = xhr.responseText;
        if (xhr.status == 401) {
            //User not found
            var tmpObj = jQuery.parseJSON(xhr.responseText);
            responseMessage = tmpObj.error.message;
        }
        console.log("login fail: " + responseMessage);

    });
}
