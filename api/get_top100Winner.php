<?php
$entityBody = file_get_contents('php://input');
$request_json = json_decode($entityBody, true);
$response_obj = array();
//Check for user / pass
$winners = $this->db->getTop100Winner();
if ($winners == false) {
	$response_obj['users'] = array();
} else {
	$response_obj['users'] = $winners;
}

echo json_encode($response_obj);
