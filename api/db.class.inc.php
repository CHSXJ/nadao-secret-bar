<?php
class db
{
	private $pdo;
	function __construct($host, $user, $pass, $dbname)
	{
		try {
			$this->pdo = new PDO("mysql:dbname={$dbname};host={$host}", $user, $pass);
		} catch (PDOException $e) {
			echo "Connection failed: " . $e->getMessage();
		}
	}

	public function query($sql)
	{
		return $this->pdo->query($sql);
	}

	public function getPdo()
	{
		return $this->pdo;
	}

	public static function fetch_assoc($result)
	{
		if ($result === false)
			return false;
		else
			return $result->fetch(PDO::FETCH_ASSOC);
	}

	public function createAccount($user, $pass, $gender, $firstname, $lastname, $province, $age)
	{
		try {
			$sql = 'INSERT INTO user (`username`, `password`, `gender`,`firstname`,`lastname`,`province`,`age`) VALUES (:user_username, :user_password, :user_gender, :user_firstname, :user_lastname, :user_province, :user_age)';

			$sth = $this->pdo->prepare($sql);
			$result = $sth->execute(
				array(
					':user_username' => $user,
					':user_password' => $pass,
					':user_gender' => $gender,
					':user_firstname' => $firstname,
					':user_lastname' => $lastname,
					':user_province' => $province,
					':user_age' => $age,
				)
			);
			return $this->pdo->lastInsertId();
		} catch (PDOException $e) {
			echo  $e->getMessage();
		}
	}

	public function getAdminUserByUsername($username)
	{
		$sql = 'SELECT * FROM user WHERE username = :user_username';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':user_username' => $username
			)
		);
		return $sth->fetch(PDO::FETCH_ASSOC);
	}

	public function getUserById($id)
	{
		$sql = 'SELECT * FROM users WHERE user_id = :user_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':user_id' => $id
			)
		);
		$row = $sth->fetch(PDO::FETCH_ASSOC);

		return $row;
	}

	public function submitAnswers($user_id, $answers, $is_correct, $correct_timestamp, $question_number)
	{
		$sql = 'INSERT INTO user_response (`user_id`, `answer`, `question_number`,`is_correct`,`correct_timestamp`) VALUES (:id, :answer, :question_number, :is_correct, :correct_timestamp)';
		$sth = $this->pdo->prepare($sql);
		$result = $sth->execute(
			array(
				':id' => $user_id,
				':answer' => $answers,
				':is_correct' => $is_correct,
				':question_number' => $question_number,
				':correct_timestamp' => $correct_timestamp,
			)
		);
		return $this->pdo->lastInsertId();
	}

	public function updateAnswers($answers, $is_correct, $correct_timestamp, $id)
	{
		$sql = 'UPDATE user_response SET answer = :answer, is_correct = :is_correct , correct_timestamp = :correct_timestamp WHERE id = :id';
		$sth = $this->pdo->prepare($sql);
		$result = $sth->execute(
			array(
				':answer' => $answers,
				':is_correct' => $is_correct,
				':correct_timestamp' => $correct_timestamp,
				':id' => $id
			)
		);
		return $result;
	}

	public function updateUserCorrectness($user_id, $number_correct, $timestamp)
	{
		$sql = 'INSERT INTO `user_correctness` (`num_correct`, `user_id`, `finish_time`) values ( :number_correct, :user_id, :finish_time) 
		ON DUPLICATE KEY UPDATE `user_id` = :user_id';
		$sth = $this->pdo->prepare($sql);
		$result = $sth->execute(
			array(
				':user_id' => $user_id,
				':number_correct' => $number_correct,
				':finish_time' => $timestamp,
			)
		);
		return $this->pdo->lastInsertId();
	}

	public function getUserAnswers($user_id)
	{
		$sql = 'SELECT *
				FROM user_response 
				WHERE user_id = :user_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':user_id' => $user_id
			)
		);
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function getUserCorrectnessExceeded($number_correct)
	{
		$sql = 'SELECT *
				FROM user_correctness 
				INNER JOIN user ON user_correctness.user_id = user.id
				WHERE num_correct >= :number_correct';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':number_correct' => $number_correct
			)
		);
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function getUserCorrectnessExceededCount($number_correct)
	{
		$sql = 'SELECT COUNT(id) as exceeded
				FROM user_correctness 
				WHERE num_correct >= :number_correct';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':number_correct' => $number_correct
			)
		);
		$row = $sth->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public function getGalleryImg($img_set)
	{
		$sql = 'SELECT *
				FROM gallery 
				WHERE image_set <= :img_set';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':img_set' => $img_set
			)
		);
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function getTrailer()
	{
		$sql = 'SELECT *
				FROM trailer';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array()
		);
		$row = $sth->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public function getUserCorrectAnswers($user_id)
	{
		$sql = 'SELECT question_number,correct_timestamp,answer
				FROM user_response 
				WHERE user_id = :user_id AND is_correct = 1';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':user_id' => $user_id
			)
		);
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		// error_log((print_r($rows, TRUE)));
		return $rows;
	}

	public function getTop100Winner()
	{
		$sql = 'SELECT num_correct, finish_time, age, firstname, lastname, gender, province
				FROM user_correctness
				INNER JOIN user ON user_correctness.user_id = user.id
				-- WHERE num_correct > 1 and finish_time IS NOT NULL
				ORDER BY finish_time ASC LIMIT 100';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array()
		);
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}

		return $rows;
	}

	public function getJobById($id)
	{
		$sql = 'SELECT *
				FROM jobs 
				LEFT JOIN customers ON jobs.customer_id = customers.customer_id
				WHERE job_id = :id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':id' => $id
			)
		);
		$row = $sth->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public function getJobLastedId()
	{
		$sql = 'SELECT MAX(job_id) AS max_id
				FROM jobs';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array()
		);
		$row = $sth->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public function getJobItemsByJobId($id)
	{
		// $sql = 'SELECT * FROM jobitems WHERE job_id = :id';
		$sql = 'SELECT * 
				FROM jobitems 
				LEFT JOIN services ON jobitems.service_id = services.service_id
				LEFT JOIN parts ON jobitems.part_id = parts.part_id
				WHERE jobitems.job_id = :id';

		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':id' => $id
			)
		);
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function createNewCustomer($name, $car_license, $car_color, $car_mile, $car_brand, $car_series, $tel, $email, $address, $tax_id, $isMember, $member_id, $member_expired)
	{
		$sql = 'INSERT INTO customers (`customer_name`, `customer_car_license`, `customer_car_color`, `customer_car_miles`, `customer_car_brand`, `customer_car_series`, `customer_tel`, `customer_email`, `customer_address`, `customer_tax_id`, `customer_isMember`, `customer_member_id`, `customer_member_expired`)
				VALUES (:name, :car_license, :car_color, :car_miles, :car_brand, :car_series, :tel, :email, :address, :tax_id, :isMember, :member_id, :member_expired)';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':name' => $name,
				':car_license' => $car_license,
				':car_color' => $car_color,
				':car_miles' => $car_mile,
				':car_brand' => $car_brand,
				':car_series' => $car_series,
				':tel' => $tel,
				':email' => $email,
				':address' => $address,
				':tax_id' => $tax_id,
				':isMember' => $isMember,
				':member_id' => $member_id,
				':member_expired' => $member_expired
			)
		);
		return $this->pdo->lastInsertId();
	}

	public function updateCustomer($id, $member_id, $name, $car_license, $car_color, $car_mile, $car_brand, $car_series, $tel, $email, $address, $tax_id, $isMember, $member_expired)
	{
		$sql = 'UPDATE `customers`
				SET customer_member_id = :customer_member_id,
					customer_name = :customer_name,
					customer_car_license = :customer_car_license,
					customer_car_color = :customer_car_color,
					customer_car_miles = :customer_car_miles,
					customer_car_brand = :customer_car_brand,
					customer_car_series = :customer_car_series,
					customer_tel = :customer_tel,
					customer_email = :customer_email,
					customer_address = :customer_address,
					customer_tax_id = :customer_tax_id,
					customer_isMember = :customer_isMember,
					customer_member_expired = :customer_member_expired
				WHERE customer_id = :customer_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':customer_id' => $id,
				':customer_member_id' => $member_id,
				':customer_name' => $name,
				':customer_car_license' => $car_license,
				':customer_car_color' => $car_color,
				':customer_car_miles' => $car_mile,
				':customer_car_brand' => $car_brand,
				':customer_car_series' => $car_series,
				':customer_tel' => $tel,
				':customer_email' => $email,
				':customer_address' => $address,
				':customer_tax_id' => $tax_id,
				':customer_isMember' => $isMember,
				':customer_member_expired' => $member_expired
			)
		);
		return $result;
	}

	public function createNewService($name)
	{
		$sql = 'INSERT INTO services (`service_name`)
				VALUES (:name)';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':name' => $name
			)
		);
		return $this->pdo->lastInsertId();
	}

	public function updateService($service_id, $name)
	{

		$sql = 'UPDATE `services` SET service_name = :name
				WHERE service_id = :service_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':name' => $name,
				':service_id' => $service_id
			)
		);
		return $result;
	}

	public function createNewJob($customer_id)
	{
		$sql = 'INSERT INTO jobs (`customer_id`, `job_status`, `job_mechanic`)
				VALUES (:customer_id, :job_status, :job_mechanic)';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':customer_id' => $customer_id,
				':job_status' => "0",
				':job_mechanic' => ""
			)
		);
		return $this->pdo->lastInsertId();
	}

	// public function createNewInvoice($job_id, $customer_id) {
	// 	$sql = 'INSERT INTO invoices (`job_id`, `customer_id`)
	// 			VALUES (:job_id, :customer_id)';
	// 	$sth = $this->pdo->prepare($sql);

	// 	$result = $sth->execute(
	// 					Array(
	// 						':job_id' => $job_id,
	// 						':customer_id' => $customer_id
	// 					)
	// 	);
	// 	return $this->pdo->lastInsertId();
	// }

	public function createNewJobItem($job_id, $part_id, $service_id, $quantity, $image, $remark)
	{
		$sql = 'INSERT INTO jobitems (`job_id`, `part_id`, `service_id`, `jobitem_quantity`, `jobitem_image`, `jobitem_remark`)
				VALUES (:job_id, :part_id, :service_id, :quantity, :jobitem_image, :remark)';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':job_id' => $job_id,
				':part_id' => $part_id,
				':service_id' => $service_id,
				':quantity' => $quantity,
				':jobitem_image' => $image,
				':remark' => $remark
			)
		);
		return $this->pdo->lastInsertId();
	}

	public function deleteJobItem($jobitem_id)
	{

		$sql = 'DELETE FROM `jobitems` WHERE jobitem_id = :jobitem_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':jobitem_id' => $jobitem_id
			)
		);

		return $result;
	}

	public function deleteJob($job_id)
	{

		$sql = 'DELETE FROM `jobs` WHERE job_id = :job_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':job_id' => $job_id
			)
		);

		return $result;
	}

	public function completeJob($job_id)
	{

		$sql = 'UPDATE `jobs` SET job_status = :job_status
				WHERE job_id = :job_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':job_id' => $job_id,
				':job_status' => "1"
			)
		);
		return $result;
	}

	public function updateMechanic($job_id, $job_mechanic)
	{

		$sql = 'UPDATE `jobs` SET job_mechanic = :job_mechanic
				WHERE job_id = :job_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':job_id' => $job_id,
				':job_mechanic' => $job_mechanic
			)
		);
		return $result;
	}

	public function updateJobNo($job_id, $job_no)
	{

		$sql = 'UPDATE `jobs` SET job_no = :job_no
				WHERE job_id = :job_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':job_id' => $job_id,
				':job_no' => $job_no
			)
		);
		return $result;
	}

	public function addPart($name, $price, $quantity, $service_id)
	{

		$sql = 'INSERT INTO parts (`part_name`, `part_price`, `part_quantity`, `service_id`)
				VALUES (:name, :price, :quantity, :service_id)';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':name' => $name,
				':price' => $price,
				':quantity' => $quantity,
				':service_id' => $service_id
			)
		);
		return $this->pdo->lastInsertId();
	}

	public function updatePart($part_id, $name, $price, $quantity)
	{

		$sql = 'UPDATE `parts` SET part_name = :name, part_price = :price, part_quantity = :quantity
				WHERE part_id = :part_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':name' => $name,
				':price' => $price,
				':quantity' => $quantity,
				':part_id' => $part_id
			)
		);
		return $result;
	}

	public function getPartsByServiceId($id)
	{
		$sql = 'SELECT * FROM parts WHERE service_id = :id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':id' => $id
			)
		);
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function getAllCustomer()
	{

		$sql = 'SELECT *  FROM customers';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute();
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function getCustomerByJobId($job_id)
	{
		$sql = 'SELECT *
				FROM customers
				LEFT JOIN jobs ON jobs.customer_id = customers.customer_id
				-- LEFT JOIN invoices ON customers.customer_id = invoices.customer_id
				WHERE jobs.job_id = :id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':id' => $job_id
			)
		);
		$row = $sth->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public function getCustomerById($customer_id)
	{
		$sql = 'SELECT *
				FROM customers
				WHERE customer_id = :id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':id' => $customer_id
			)
		);
		$row = $sth->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public function getExpCustomers()
	{

		$sql = 'SELECT *
				FROM customers
				WHERE MONTH(customer_member_expired) = MONTH(NOW())
				AND YEAR(customer_member_expired) = YEAR(NOW())
				ORDER BY customer_member_expired ASC';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute();
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function getMonthlyCheckCustomers()
	{

		$sql = 'SELECT *
				FROM customers
				WHERE MONTH(customer_monthly_check) != MONTH(NOW())
				AND YEAR(customer_monthly_check) <= YEAR(NOW())
				ORDER BY customer_monthly_check ASC';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute();
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function checkMonthlyById($id)
	{

		$sql = 'UPDATE `customers`
				SET customer_monthly_check = TIME(NOW())
				WHERE customer_id = :customer_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':customer_id' => $id
			)
		);
		return $result;
	}

	public function getAllServices()
	{

		$sql = 'SELECT *  FROM services';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute();
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function getAllParts()
	{

		$sql = 'SELECT *
				FROM parts';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute();
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function getAllJob()
	{

		$sql = 'SELECT *
				FROM jobs
				LEFT JOIN customers
				ON jobs.customer_id = customers.customer_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute();
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function getJobByCustomerId($customer_id)
	{

		$sql = 'SELECT *
				FROM jobs
				WHERE customer_id = :id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':id' => $customer_id
			)
		);
		$rows = array();
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function updateJobDiscount($job_id, $job_discount, $job_amount, $job_no, $job_method)
	{

		$sql = 'UPDATE `jobs` SET job_discount = :job_discount, job_amount = :job_amount, job_no = :job_no, job_method = :job_method
				WHERE job_id = :job_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':job_discount' => $job_discount,
				':job_amount' => $job_amount,
				':job_no' => $job_no,
				':job_id' => $job_id,
				':job_method' => $job_method
			)
		);
		return $result;
	}

	public function updateJobPaid($job_id, $job_isPaid)
	{

		$sql = 'UPDATE `jobs` SET job_isPaid = :job_isPaid
				WHERE job_id = :job_id';
		$sth = $this->pdo->prepare($sql);

		$result = $sth->execute(
			array(
				':job_isPaid' => $job_isPaid,
				':job_id' => $job_id
			)
		);
		return $result;
	}
}
