<?php
$entityBody = file_get_contents('php://input');
$request_json = json_decode($entityBody, true);
$response_obj = array();


if ($request_json['number_correct'] != '') {
	//Check for user / pass
	$users_old_answer = $this->db->getUserCorrectnessExceeded(
		$request_json['number_correct'],
	);
	if ($users_old_answer == false) {
		$response_obj['users'] = array();
	} else {
		$response_obj['users'] = $users_old_answer;
	}
} else {
	$this->httpError(400);
	$response_obj['code'] = 'Bad request';
	$response_obj['error']['message'] = 'Please specify number of corrected questions.';
}

echo json_encode($response_obj);
