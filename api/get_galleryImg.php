<?php
$entityBody = file_get_contents('php://input');
$request_json = json_decode($entityBody, true);
$response_obj = array();
$response_obj['images'] = array();

for ($i = 9; $i > 1; $i = $i - 3) {
	$user_exceeded = $this->db->getUserCorrectnessExceededCount(
		$i,
	);
	if ($user_exceeded['exceeded'] > 99) {
		$img_set = $this->db->getGalleryImg(
			$i / 3,
		);
		$response_obj['images'] = $img_set;
		break;
	}
}

echo json_encode($response_obj);
