<?php
$entityBody = file_get_contents('php://input');
$request_json = json_decode($entityBody, true);
$response_obj = array();


if ($request_json['user_id'] != '') {
	//Check for user / pass
	$users_old_answer = $this->db->getUserAnswers(
		$request_json['user_id'],
	);
	if ($users_old_answer == false) {
		$response_obj['answers'] = array();
	} else {
		$response_obj['answers'] = $users_old_answer;
	}
} else {
	$this->httpError(401);
	$response_obj['code'] = 'USER_NOYFOUND';
	$response_obj['error']['message'] = 'Username id be empty.';
}

echo json_encode($response_obj);
