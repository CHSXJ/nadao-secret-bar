<?php
$entityBody = file_get_contents('php://input');
$request_json = json_decode($entityBody, true);
$response_obj = array();


if ($request_json['user'] != '') {
	//Check for user / pass
	$users = $this->db->getAdminUserByUsername($request_json['user']);
	if ($users == false) {
		$this->httpError(401);
		$response_obj['code'] = 'USER_NOT_FOUND';
		$response_obj['error']['message'] = 'User not found';
		echo "USER_NOT_FOUND";
	}
	// else if($request_json['pass'] == $users['user_password']){
	else if (md5($request_json['pass'] . 'pcher' . $request_json['user']) == $users['password']) {
		$response_obj['code'] = 'OK';
		$response_obj['authen']['session'] = session_id();
		// $_SESSION['api']['backend']['is_login'] = true;
		// $_SESSION['api']['backend']['admin_id'] = $users['id'];
		$_SESSION['api']['is_login'] = true;
		$_SESSION['api']['user_id'] = $users['id'];
	} else {
		$this->httpError(401);
		$response_obj['code'] = 'WRONG_USER_PASS';
		$response_obj['error']['message'] = 'Wrong username or password.';
	}
} else {
	$this->httpError(401);
	$response_obj['code'] = 'EMPTY_USER';
	$response_obj['error']['message'] = 'Username can\'t be empty.';
}

echo json_encode($response_obj);
