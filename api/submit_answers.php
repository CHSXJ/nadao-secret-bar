<?php

function findMatchQuestion($questionNumber, $answers)
{
	if ($answers === null) {
		return null;
	}
	foreach ($answers as $answer) {
		if ($answer['question_number'] == $questionNumber) {
			return $answer;
		}
	}
	return null;
}
$entityBody = file_get_contents('php://input');
$request_json = json_decode($entityBody, true);
$response_obj = array();
$date = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
// Hard code correct answer for each question
$correct_answer = array('aaa', 'bbb', 'ccc', 'ddd', 'eee', 'fff', 'ggg', 'hhh', 'iii');
$correct_cnt = 0;
if ($request_json['user_id'] != '') {
	$users_old_answer = $this->db->getUserAnswers(
		$request_json['user_id'],
	);
	if (count((array)$users_old_answer) > 0) {
		foreach ($users_old_answer as $answer) {
			$focus_answer = findMatchQuestion($answer['question_number'], $request_json['answers']);
			if ($focus_answer != null) {
				if ($focus_answer['answer'] == $correct_answer[intval($focus_answer['question_number']) - 1]) {
					$correct_cnt++;
					$answers = $this->db->updateAnswers(
						$focus_answer['answer'],
						true,
						$date->format('Y-m-d H:i:s'),
						$answer['id']
					);
				} else {
					$answers = $this->db->updateAnswers(
						$focus_answer['answer'],
						false,
						null,
						$answer['id']
					);
				}
			}
		}
	} else {
		for ($i = 0; $i < 9; $i++) {
			if (findMatchQuestion($i + 1, $request_json['answers']) != null) {
				if ($request_json['answers'][$i]['answer'] == $correct_answer[intval($request_json['answers'][$i]['question_number']) - 1]) {
					$correct_cnt++;
					$answers = $this->db->submitAnswers(
						$request_json['user_id'],
						$request_json['answers'][$i]['answer'],
						true,
						$date->format('Y-m-d H:i:s'),
						$request_json['answers'][$i]['question_number']
					);
				} else {
					$answers = $this->db->submitAnswers(
						$request_json['user_id'],
						$request_json['answers'][$i]['answer'],
						false,
						null,
						$request_json['answers'][$i]['question_number']
					);
				}
			} else {
				$answers = $this->db->submitAnswers(
					$request_json['user_id'],
					null,
					false,
					null,
					$i + 1
				);
			}
		}
	}
	if ($correct_cnt == 9) {
		$answers = $this->db->updateUserCorrectness(
			$request_json['user_id'],
			$correct_cnt,
			$date->format('Y-m-d H:i:s'),
		);
	} else {
		$answers = $this->db->updateUserCorrectness(
			$request_json['user_id'],
			$correct_cnt,
			null,
		);
	}
} else {
	$this->httpError(401);
	$response_obj['code'] = 'Unauthorized';
	$response_obj['error']['message'] = 'Please login.';
}

echo json_encode($response_obj);
