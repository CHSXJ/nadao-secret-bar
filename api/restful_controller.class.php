<?php
class RESTfulController
{

	private $routeList;
	private $basePath;
	private $db;

	public function RESTfulController($baseUrl = '')
	{
		@session_start();
		$this->routeList = array();
		$this->basePath = $baseUrl;
	}

	public function setDB()
	{
		$this->db = new db('localhost', 'root', '', 'chstdio_bar');
	}

	public function addRoute($httpVerb, $pathRegEx, $targetScript)
	{
		$this->routeList[$httpVerb][] = array('pathRegEx' => $pathRegEx, 'targetScript' => $targetScript);
	}

	public function printRoute()
	{
		foreach ($this->routeList as $method => $routeDetails) {
			foreach ($routeDetails as $routeDetail) {
				echo "{$method} {$routeDetail['pathRegEx']} -> {$routeDetail['targetScript']}<br />\n";
			}
		}
	}

	//Execute controller by finding route and execute target script
	public function execute()
	{
		$isFound = false;
		$urlData = parse_url($_SERVER['REQUEST_URI']);

		//Check for HTTP VERB
		if (array_key_exists($_SERVER['REQUEST_METHOD'], $this->routeList)) {
			$execScript = '';
			foreach ($this->routeList[$_SERVER['REQUEST_METHOD']] as $route) {
				//Perform regex 
				//1st clean urp
				$routeData = str_replace('/', '\/', $this->basePath . $route['pathRegEx']);
				//2nd change {xxx} variable to regex
				$routeData = preg_replace('/{[a-zA-Z0-9_]+}/', '([a-zA-Z0-9_]+)', $routeData);
				//echo "Pattern:".$routeData."<br />";


				//Perform match here...
				if (preg_match('/' . $routeData . '$/', $urlData['path'])) {
					$isFound = true;
					$execScript = $route['targetScript'];
					//echo "Matched execute script ".$route['targetScript'];
					//Matched!
					break;
				}
			}
		} else {
			//Not found
		}


		if ($isFound) {
			if (file_exists($execScript))
				require_once($execScript);
			else {
				$this->httpError(500);
				echo "ERROR: target script not found - {$execScript}";
			}
			die();
		} else {
			$this->httpError(404);
			echo "ERROR: Request URL not found for {$_SERVER['REQUEST_METHOD']} {$_SERVER['REQUEST_URI']}";
			die();
		}
	}

	private function httpError($statusCode)
	{
		$status_codes = array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			102 => 'Processing',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			207 => 'Multi-Status',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			307 => 'Temporary Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			422 => 'Unprocessable Entity',
			423 => 'Locked',
			424 => 'Failed Dependency',
			426 => 'Upgrade Required',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			506 => 'Variant Also Negotiates',
			507 => 'Insufficient Storage',
			509 => 'Bandwidth Limit Exceeded',
			510 => 'Not Extended'
		);

		if ($status_codes[$statusCode] !== null) {
			$status_string = $statusCode . ' ' . $status_codes[$statusCode];
			header($_SERVER['SERVER_PROTOCOL'] . ' ' . $status_string, true, $statusCode);
		}
	}
}
