<?php
$entityBody = file_get_contents('php://input');
$request_json = json_decode($entityBody, true);
$response_obj = array();
$response_obj['trailer_url'] = '';

$user_exceeded = $this->db->getUserCorrectnessExceededCount(
	9,
);
if ($user_exceeded['exceeded'] > 99) {
	$img_set = $this->db->getTrailer();
	$response_obj['trailer_url'] = $img_set['trailer_url'];
}

echo json_encode($response_obj);
