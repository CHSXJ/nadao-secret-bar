<?php
define('security_check', 1);
require_once('load.inc.php');
$rest = new RESTfulController('/api');
// $db = new db(config::$mysql_host, config::$mysql_user, config::$mysql_pass, config::$mysql_db);

$rest->setDB();
//config routes
$rest->addRoute('POST', '/user/validateCredential', 'user_validateCredential.php');
$rest->addRoute('POST', '/user/register', 'user_register.php');
$rest->addRoute('POST', '/customers/create', 'customer_create.php');
$rest->addRoute('POST', '/customers/update', 'customer_update.php');
$rest->addRoute('POST', '/services/create', 'service_create.php');
$rest->addRoute('POST', '/parts/create', 'part_create.php');
$rest->addRoute('POST', '/jobs/items/create', 'jobitem_create.php');
$rest->addRoute('POST', '/jobs/items/delete', 'jobitem_create.php');
$rest->addRoute('POST', '/jobs/complete', 'jobitem_create.php');
$rest->addRoute('POST', '/jobs/updateMechanic', 'jobitem_create.php');
$rest->addRoute('POST', '/jobs/updateJobNo', 'jobitem_create.php');
$rest->addRoute('POST', '/jobs/create', 'job_create.php');
$rest->addRoute('POST', '/jobs/delete', 'job_create.php');
$rest->addRoute('POST', '/upload', 'upload.php');
$rest->addRoute('POST', '/test', 'test.php');
$rest->addRoute('POST', '/submitAnswers', 'submit_answers.php');
$rest->addRoute('POST', '/getAnswers', 'get_userAnswers.php');
$rest->addRoute('POST', '/getUserCorrectExceed', 'get_userCorrectnessExceeded.php');
$rest->addRoute('POST', '/getCorrectAnswers', 'get_userCorrectAnswers.php');
$rest->addRoute('POST', '/getImgGallery', 'get_galleryImg.php');
$rest->addRoute('POST', '/getTrailerURL', 'get_trailerURL.php');
$rest->addRoute('POST', '/getTop100', 'get_top100Winner.php');

// $rest->addRoute('POST', '/invoices/create', 'invoice_create.php');

//Debug
// if ($_GET['debug'] == true) {
// 	$rest->printRoute();
// 	die();
// }

//Execute controller
$rest->execute();
/**** Code will not get execute below this line ****/
