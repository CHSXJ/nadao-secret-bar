<?php
$entityBody = file_get_contents('php://input');
$request_json = json_decode($entityBody, true);
$response_obj = array();
if ($request_json['user'] != '' && $request_json['pass'] != '') {
	//Check for user / pass
	$users = $this->db->createAccount(
		$request_json['user'],
		md5($request_json['pass'] . 'pcher' . $request_json['user']),
		$request_json['gender'],
		$request_json['firstname'],
		$request_json['lastname'],
		$request_json['province'],
		$request_json['age'],
	);
} else {
	$this->httpError(401);
	$response_obj['code'] = 'EMPTY_USER';
	$response_obj['error']['message'] = 'Username can\'t be empty.';
}

echo json_encode($response_obj);
