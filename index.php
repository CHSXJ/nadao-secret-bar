<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- bootstrap 4.3.1 -->
  <link rel="stylesheet" href="css/vendor/bootstrap.min.css">
  <!-- styles -->
  <link rel="stylesheet" href="css/styles.min.css">
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="plugins/iCheck/icheck.min.js"></script>
  <script src="request.js"></script>
  <!-- favicon -->
  <link rel="icon" href="img/favicon.ico">
  <title>Home</title>
  <script>
    function submitAnswer() {
      $.ajax({
        url: '/api/submitAnswers',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
          user_id: 1,
          answers: [{
              question_number: 1,
              answer: 'aaa'
            },
            {
              question_number: 2,
              answer: 'ggg'
            },
            {
              question_number: 3,
              answer: 'ccc'
            },
            {
              question_number: 4,
              answer: 'ddd'
            },
            {
              question_number: 7,
              answer: 'ddd'
            }
          ],
        }),
        dataType: 'json'
      }).done(function(data) {
        console.log("done");
        console.log(data);
        window.location = 'index.php';
      }).fail(function(xhr, ajaxOptions, thrownError) {
        //xhr.responseText
        var responseMessage = xhr.responseText;
        if (xhr.status == 401) {
          //User not found
          var tmpObj = jQuery.parseJSON(xhr.responseText);
          responseMessage = tmpObj.error.message;
        }
        console.log("login fail: " + responseMessage);

      });
    }
  </script>
</head>

<body>

  <!-- LANDING -->
  <div class="landing">
    <!-- LANDING DECORATION -->
    <div class="landing-decoration"></div>
    <!-- /LANDING DECORATION -->

    <!-- LANDING INFO -->
    <div class="landing-info">
      <!-- LOGO -->
      <div class="logo">
        <!-- ICON LOGO VIKINGER -->
        <svg class="icon-logo-vikinger">
          <use xlink:href="#svg-logo-vikinger"></use>
        </svg>
        <!-- /ICON LOGO VIKINGER -->
      </div>
      <!-- /LOGO -->

      <!-- LANDING INFO PRETITLE -->
      <h2 class="landing-info-pretitle">Welcome to</h2>
      <!-- /LANDING INFO PRETITLE -->

      <!-- LANDING INFO TITLE -->
      <h1 class="landing-info-title">secret bar</h1>
      <!-- /LANDING INFO TITLE -->

      <!-- LANDING INFO TEXT -->
      <p class="landing-info-text">bla bla bla bla bla bla &amp; bla bla bla bla bla blabla bla bla bla bla blabla bla
        bla bla bla bla!</p>
      <!-- /LANDING INFO TEXT -->

      <!-- TAB SWITCH -->
      <div class="tab-switch">
        <!-- TAB SWITCH BUTTON -->
        <p class="tab-switch-button login-register-form-trigger">Login</p>
        <!-- /TAB SWITCH BUTTON -->

        <!-- TAB SWITCH BUTTON -->
        <p class="tab-switch-button login-register-form-trigger">Register</p>
        <!-- /TAB SWITCH BUTTON -->
      </div>
      <!-- /TAB SWITCH -->
    </div>
    <!-- /LANDING INFO -->

    <!-- LANDING FORM -->
    <div class="landing-form">
      <!-- FORM BOX -->
      <div class="form-box login-register-form-element">

        <!-- FORM BOX TITLE -->
        <h2 class="form-box-title">Account Login</h2>
        <!-- /FORM BOX TITLE -->

        <!-- FORM -->
        <!-- FORM ROW -->
        <div class="form-row">
          <!-- FORM ITEM -->
          <div class="form-item">
            <!-- FORM INPUT -->
            <div class="form-input">
              <label for="login-username">Username</label>
              <input type="text" id="login-username" name="login_username">
            </div>
            <!-- /FORM INPUT -->
          </div>
          <!-- /FORM ITEM -->
        </div>
        <!-- /FORM ROW -->

        <!-- FORM ROW -->
        <div class="form-row">
          <!-- FORM ITEM -->
          <div class="form-item">
            <!-- FORM INPUT -->
            <div class="form-input">
              <label for="login-password">Password</label>
              <input type="password" id="login-password" name="login_password">
            </div>
            <!-- /FORM INPUT -->
          </div>
          <!-- /FORM ITEM -->
        </div>
        <!-- /FORM ROW -->

        <!-- FORM ROW -->
        <div class="form-row space-between">
          <!-- FORM ITEM -->
          <div class="form-item">
            <!-- CHECKBOX WRAP -->
            <div class="checkbox-wrap">
              <input type="checkbox" id="login-remember" name="login_remember" checked>
              <!-- CHECKBOX BOX -->
              <div class="checkbox-box">
                <!-- ICON CROSS -->
                <svg class="icon-cross">
                  <use xlink:href="#svg-cross"></use>
                </svg>
                <!-- /ICON CROSS -->
              </div>
              <!-- /CHECKBOX BOX -->
              <label for="login-remember">Remember Me</label>
            </div>
            <!-- /CHECKBOX WRAP -->
          </div>
          <!-- /FORM ITEM -->

          <!-- FORM ITEM -->
          <div class="form-item">
            <!-- FORM LINK -->
            <a class="form-link" href="#">Forgot Password?</a>
            <!-- /FORM LINK -->
          </div>
          <!-- /FORM ITEM -->
        </div>
        <!-- /FORM ROW -->

        <!-- FORM ROW -->
        <div class="form-row">
          <!-- FORM ITEM -->
          <div class="form-item">
            <!-- BUTTON -->
            <button onclick="login()" class="button medium secondary">Login to your Account!</button>
            <!-- /BUTTON -->
          </div>
          <!-- /FORM ITEM -->
        </div>
        <!-- /FORM ROW -->
        <!-- /FORM -->

      </div>
      <!-- /FORM BOX -->

      <!-- FORM BOX -->
      <div class="form-box login-register-form-element">
        <!-- FORM BOX DECORATION -->
        <img class="form-box-decoration" src="img/landing/rocket.png" alt="rocket">
        <!-- /FORM BOX DECORATION -->

        <!-- FORM BOX TITLE -->
        <h2 class="form-box-title">Create your Account!</h2>
        <!-- /FORM BOX TITLE -->

        <!-- FORM -->
        <div class="form-row">
          <div class="form-item">
            <div class="form-input">
              <label for="register-firstname">First name</label>
              <input type="text" id="register-firstname" name="register_firstname">
            </div>
          </div>
        </div>

        <div class="form-row">
          <div class="form-item">
            <div class="form-input">
              <label for="register-lastname">Last name</label>
              <input type="text" id="register-lastname" name="register_lastname">
            </div>
          </div>
        </div>

        <!-- FORM ROW -->
        <div class="form-row">
          <!-- FORM ITEM -->
          <div class="form-item">
            <!-- FORM INPUT -->
            <div class="form-input">
              <label for="register-username">Username</label>
              <input type="text" id="register-username" name="register_username">
            </div>
            <!-- /FORM INPUT -->
          </div>
          <!-- /FORM ITEM -->
        </div>
        <!-- /FORM ROW -->

        <!-- FORM ROW -->
        <div class="form-row">
          <!-- FORM ITEM -->
          <div class="form-item">
            <!-- FORM INPUT -->
            <div class="form-input">
              <label for="register-password">Password</label>
              <input type="password" id="register-password" name="register_password">
            </div>
            <!-- /FORM INPUT -->
          </div>
          <!-- /FORM ITEM -->
        </div>
        <!-- /FORM ROW -->

        <!-- FORM ROW -->
        <div class="form-row">
          <!-- FORM ITEM -->
          <div class="form-item">
            <!-- FORM INPUT -->
            <div class="form-input">
              <label for="register-password-repeat">Repeat Password</label>
              <input type="password" id="register-password-repeat" name="register_password_repeat">
            </div>
            <!-- /FORM INPUT -->
          </div>
          <!-- /FORM ITEM -->
        </div>
        <!-- /FORM ROW -->

        <!-- FORM ROW -->
        <div class="form-row">
          <!-- FORM ITEM -->
          <div class="form-item">
            <!-- CHECKBOX WRAP -->
            <div class="checkbox-wrap">
              <input type="checkbox" id="register-newsletter" name="register_newsletter" checked>
              <!-- CHECKBOX BOX -->
              <div class="checkbox-box">
                <!-- ICON CROSS -->
                <svg class="icon-cross">
                  <use xlink:href="#svg-cross"></use>
                </svg>
                <!-- /ICON CROSS -->
              </div>
              <!-- /CHECKBOX BOX -->
              <label for="register-newsletter">Send me news and updates via email</label>
            </div>
            <!-- /CHECKBOX WRAP -->
          </div>
          <!-- /FORM ITEM -->
        </div>
        <!-- /FORM ROW -->

        <!-- FORM ROW -->
        <div class="form-row">
          <!-- FORM ITEM -->
          <div class="form-item">
            <!-- BUTTON -->
            <button onclick="getAnswers()" class="button medium primary">Register Now!</button>
            <!-- /BUTTON -->
          </div>
          <!-- /FORM ITEM -->
        </div>
        <!-- /FORM ROW -->
        <!-- /FORM -->

        <!-- FORM TEXT -->
        <p class="form-text">You'll receive a confirmation email in your inbox with a link to activate your account. If
          you have any problems, <a href="#">contact us</a>!</p>
        <!-- /FORM TEXT -->
      </div>
      <!-- /FORM BOX -->
    </div>
    <!-- /LANDING FORM -->
  </div>
  <!-- /LANDING -->


  <!-- app -->
  <script src="js/utils/app.js"></script>
  <!-- XM_Plugins -->
  <script src="js/vendor/xm_plugins.min.js"></script>
  <!-- form.utils -->
  <script src="js/form/form.utils.js"></script>
  <!-- landing.tabs -->
  <script src="js/landing/landing.tabs.js"></script>
  <!-- SVG icons -->
  <script src="js/utils/svg-loader.js"></script>
</body>

</html>
